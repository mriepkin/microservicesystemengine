#from distutils.core import setup
import setuptools

with open('requirements.txt') as f:
    required = f.read().splitlines()

setuptools.setup(
    name="microservice_system_engine",
    version="1.69",
    description="This is framework for service communication and message handling",
    author="Maksym Riepkin",
    author_email="maksym.riepkin@mycredit.ua",
    packages=[
        'microservice_system_engine',
        'microservice_system_engine.abstract_classes',
        'microservice_system_engine.caching',
        'microservice_system_engine.configs',
        'microservice_system_engine.databases',
        'microservice_system_engine.loggers',
        'microservice_system_engine.messaging',
        'microservice_system_engine.messaging.handlers',
        'microservice_system_engine.utils',
    ],
    install_requires=required
)

