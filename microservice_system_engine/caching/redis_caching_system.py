from microservice_system_engine.abstract_classes.abstract_caching_system import AbstractCachingSystem
import redis
import json
from microservice_system_engine.configs.redis_config import RedisConfig


class RedisCachingSystem(AbstractCachingSystem):
    __instance: 'RedisCachingSystem' = None

    def __init__(self, config: RedisConfig):
        if not RedisCachingSystem.__instance:
            self.__connection = redis.Redis(host=config.host, port=config.port)
            RedisCachingSystem.__instance = self

    def put(self, unique_key, value):
        if type(value) is dict or type(value) is list:
            value = json.dumps(value, ensure_ascii=False)
        return self.__connection.set(unique_key, value)

    def get(self, unique_key):
        value = self.__connection.get(unique_key)
        if value is not None:
            value = value.decode("utf-8")
        if value is None or type(value) is not str:
            return value
        try:
            value = json.loads(value)
            return value
        except:
            return value

    def delete(self, unique_key):
        self.__connection.delete(unique_key)

    def close_connections(self):
        self.__connection.close()

    @staticmethod
    def get_instance():
        return RedisCachingSystem.__instance