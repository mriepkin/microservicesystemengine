

class CassandraConfig:
    def __init__(self, username: str, password: str, cluster_host: str, port: int, key_space: str):
        self.username = username
        self.password = password
        self.cluster_host = cluster_host
        self.port = port
        self.key_space = key_space