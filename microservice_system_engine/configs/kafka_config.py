from typing import List


class KafkaConfig:
    class Consumer:
        def __init__(self,
                     topic: str,
                     server: List[str],
                     value_serializer,
                     enable_auto_commit: bool = True,
                     auto_offset_reset: str = 'earliest',
                     auto_commit_interval_ms: int = 1000,
                     group_id: str = 'counters'):
            self.topic = topic
            self.server = server
            self.enable_auto_commit = enable_auto_commit
            self.auto_offset_reset = auto_offset_reset
            self.auto_commit_interval_ms = auto_commit_interval_ms
            self.group_id = group_id
            self.value_serializer = value_serializer


    class Producer:
        def __init__(self, server: List[str], value_serializer):
            self.server = server
            self.value_serializer = value_serializer


    def __init__(self, producer: Producer, consumer: Consumer):
        self.producer = producer
        self.consumer = consumer
