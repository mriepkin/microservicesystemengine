from microservice_system_engine.abstract_classes.abstract_logger import AbstractLoggerConfig


class LocalLoggerConfig(AbstractLoggerConfig):
    def __init__(self, min_log_level: str):
        super(LocalLoggerConfig, self).__init__(min_log_level)