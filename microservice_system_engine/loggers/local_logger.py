from microservice_system_engine.abstract_classes.abstract_logger import AbstractLogger
import logging
from microservice_system_engine.utils.logging_level import LoggingLevel


class LocalLogger(AbstractLogger):

    def __init__(self, config):
        super(LocalLogger, self).__init__(config)
        self._logger = logging.getLogger(__name__)

    def log(self, log_level: LoggingLevel, log_name: str, message: str):
        if log_level.value < self._min_log_level.value:
            return
        if log_level.value == LoggingLevel.DEBUG_HIGH.value:
            self._logger.debug(message)
            return
        if log_level.value == LoggingLevel.DEBUG.value:
            self._logger.debug(message)
            return
        if log_level.value == LoggingLevel.INFO.value:
            self._logger.info(message)
            return
        if log_level.value == LoggingLevel.WARN.value:
            self._logger.warning(message)
            return
        if log_level.value == LoggingLevel.ERROR.value:
            self._logger.error(message)
            return
        if log_level.value == LoggingLevel.FATAL.value:
            self._logger.critical(message)
            return

