from typing import List
from microservice_system_engine.abstract_classes.abstract_logger import AbstractLogger
from microservice_system_engine.abstract_classes.abstract_logger_manager import AbstractLoggerManager


class LoggerManager(AbstractLoggerManager):
    _active_loggers: List[AbstractLogger] = [
    ]
