from microservice_system_engine.abstract_classes.abstract_db_connector import AbstractDbConnector, AbstractDbDataWrapper
from cassandra.cluster import Cluster, Session
from cassandra.auth import PlainTextAuthProvider
from typing import List
from microservice_system_engine.configs.cassandra_config import CassandraConfig


class CassandraConnector(AbstractDbConnector):

    def __init__(self, config: CassandraConfig):
        super(CassandraConnector, self).__init__()
        self.__auth_provider = PlainTextAuthProvider(username=config.username, password=config.password)
        self.__cluster = Cluster(config.cluster_host,
                                 port=config.port,
                                 auth_provider=self.__auth_provider)
        self.__session = self.__cluster.connect(config.key_space)

    def query_single(self, query_string: str) -> AbstractDbDataWrapper:
        pass

    def query_multiple(self, query_string: str) -> List[AbstractDbDataWrapper]:
        pass

    def insert(self, query_string: str) -> None:
        pass

    def connect(self):
        pass

    def disconnect(self):
        self.__session.shutdown()
        self.__cluster.shutdown()
