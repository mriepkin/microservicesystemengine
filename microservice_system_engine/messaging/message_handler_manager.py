from typing import Dict

from microservice_system_engine.abstract_classes.abstract_message_manager import AbstractMessageManager
from microservice_system_engine.abstract_classes.abstract_message_handler_manager import AbstractMessageHandlerManager, AbstractMessageHandler
from microservice_system_engine.utils.message_codes import MessageCodes

from microservice_system_engine.messaging.handlers.features_request_handler import FeatureRequestHandler
from microservice_system_engine.messaging.handlers.features_response_handler import FeatureResponseHandler
from microservice_system_engine.abstract_classes.abstract_caching_system import AbstractCachingSystem


class MessageHandlerManager(AbstractMessageHandlerManager):
    _handlers: Dict[MessageCodes, AbstractMessageHandler] = {
    }

    def __init__(self, message_manager: AbstractMessageManager):
        self._message_manager: AbstractMessageManager = message_manager

    def handle_messages(self):
        message = self._message_manager.get_message_to_process()
        while message is not None:
            MessageHandlerManager._handle_message(message)
            message = self._message_manager.get_message_to_process()
