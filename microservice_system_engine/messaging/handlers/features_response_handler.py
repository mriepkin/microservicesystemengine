from microservice_system_engine.abstract_classes.abstract_message_handler_manager import AbstractMessageHandler
from microservice_system_engine.messaging.features_messages import FeaturesResponse
from microservice_system_engine.abstract_classes.abstract_transformer import AbstractTransformer
from microservice_system_engine.abstract_classes.abstract_caching_system import AbstractCachingSystem


class FeatureResponseHandler(AbstractMessageHandler):
    __transformer: AbstractTransformer = None
    __caching_system: AbstractCachingSystem = None

    @staticmethod
    def assign_caching_system(caching_system: AbstractCachingSystem):
        FeatureResponseHandler.__caching_system = caching_system

    @staticmethod
    def assign_transformer(transformer: AbstractTransformer):
        FeatureResponseHandler.__transformer = transformer

    @staticmethod
    def handle(json_message: dict) -> None:
        message = FeaturesResponse.create_instance(json_message)
        data = FeatureResponseHandler.__caching_system.get(message.request_id)
        if data is None:
            #TODO: log
            return
        FeatureResponseHandler.__caching_system.delete(message.request_id)
        for key in message.data:
            if key not in data:
                data[key] = message.data[key]
        print(data)