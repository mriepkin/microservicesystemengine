from microservice_system_engine.abstract_classes.abstract_message_handler_manager import AbstractMessageHandler
from microservice_system_engine.messaging.features_messages import FeaturesRequest
from microservice_system_engine.abstract_classes.abstract_transformer import AbstractTransformer
from microservice_system_engine.abstract_classes.abstract_message_manager import AbstractMessageManager
from microservice_system_engine.messaging.features_messages import FeaturesResponse


class FeatureRequestHandler(AbstractMessageHandler):
    __transformer: AbstractTransformer = None
    __message_manager: AbstractMessageManager = None

    @staticmethod
    def assign_message_manager(message_manager: AbstractMessageManager):
        FeatureRequestHandler.__message_manager = message_manager

    @staticmethod
    def assign_transformer(transformer: AbstractTransformer):
        FeatureRequestHandler.__transformer = transformer

    @staticmethod
    def handle(json_message: dict) -> None:
        message = FeaturesRequest.create_instance(json_message)
        data, generated_columns = FeatureRequestHandler.__transformer.execute(message.data, message.aliases[0])
        response = FeaturesResponse(message.request_id, FeatureRequestHandler.__transformer.get_transformer_name(), generated_columns, data)
        FeatureRequestHandler.__message_manager.send_message(message.callback_topic, response)