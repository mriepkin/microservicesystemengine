from microservice_system_engine.abstract_classes.abstract_messaging_system import AbstractMessagingSystem
from kafka import KafkaConsumer, KafkaProducer
from microservice_system_engine.configs.kafka_config import KafkaConfig


class KafkaHandler(AbstractMessagingSystem):

    def __init__(self, config: KafkaConfig):
        super(KafkaHandler, self).__init__()
        self._listening_topic = config.consumer.topic
        self._producer = KafkaProducer(bootstrap_servers=config.producer.server
                                       , value_serializer=config.producer.value_serializer)
        # self._consumer = KafkaConsumer(config['consumer']['consumer-topic']
        #                                , bootstrap_servers=config['consumer']['kafka-server']
        #                                , auto_offset_reset=config['consumer']['auto-commit']
        #                                , enable_auto_commit=config['consumer']['enable-auto-commit']
        #                                , auto_commit_interval_ms=config['consumer']['auto-commit-interval-ms']
        #                                , group_id=config['consumer']['group-id']
        #                                , value_deserializer=lambda x: json.loads(x.decode('utf-8')))
        self._consumer = KafkaConsumer(config.consumer.topic
                                       , bootstrap_servers=config.consumer.server
                                       , auto_offset_reset=config.consumer.auto_offset_reset
                                       , value_deserializer=config.consumer.value_serializer)
        KafkaHandler.__instance = self

    def consume_message(self) -> dict:
        topic_partitions = self._consumer.poll(1500)
        return topic_partitions

    def produce_message(self, topic, message):
        self._producer.send(topic, message)

    def flush_messages(self):
        self._producer.flush()

    def close_connections(self):
        self._producer.close()
        self._consumer.close()

    def get_listening_topic(self):
        return self._listening_topic
