from typing import Dict, List
from microservice_system_engine.abstract_classes.abstract_messaging_system import AbstractMessagingSystem
from microservice_system_engine.abstract_classes.abstract_message import AbstractMessage
from microservice_system_engine.abstract_classes.abstract_message_manager import AbstractMessageManager
from microservice_system_engine.abstract_classes.abstract_logger_manager import AbstractLoggerManager


class MessageManager(AbstractMessageManager):
    __messages_to_process: List[dict] = []
    __messages_to_send: Dict[str, List[AbstractMessage]] = {}

    def __init__(self, logger: AbstractLoggerManager, messaging_system: AbstractMessagingSystem):
        self.__logger = logger
        self.__messaging_system = messaging_system

    def get_listening_destination(self):
        return self.__messaging_system.get_listening_topic()

    def consume_messages(self):
        topic_partitions = self.__messaging_system.consume_message()
        for topic_partition_key in topic_partitions.keys():
            for message in topic_partitions[topic_partition_key]:
                MessageManager.__messages_to_process.append(message.value)

    def send_message(self, topic: str, message: AbstractMessage):
        if topic not in MessageManager.__messages_to_send:
            MessageManager.__messages_to_send[topic] = []
        MessageManager.__messages_to_send[topic].append( message)

    def flush_messages(self):
        kafka_instance = self.__messaging_system
        for destination_topic in MessageManager.__messages_to_send.keys():
            messages = MessageManager.__messages_to_send[destination_topic]
            for message in messages:
                kafka_instance.produce_message(destination_topic, message.to_json())
            MessageManager.__messages_to_send[destination_topic] = []
        kafka_instance.flush_messages()

    def get_message_to_process(self):
        if len(MessageManager.__messages_to_process) == 0:
            return None
        return MessageManager.__messages_to_process.pop(0)
