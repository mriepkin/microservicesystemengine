from microservice_system_engine.abstract_classes.abstract_message import AbstractMessage
from typing import List
from microservice_system_engine.utils.message_codes import MessageCodes
from microservice_system_engine.utils.feature_data import FeatureData
from microservice_system_engine.utils.feature_data_cache import FeatureDataCache
from microservice_system_engine.utils import Alias
import pandas as pd


class FeaturesRequest(AbstractMessage):
    def __init__(self, request_id: str, aliases: List[Alias], data: pd.DataFrame, callback_topic: str):
        super(FeaturesRequest, self).__init__(request_id, MessageCodes.FEATURES_REQUEST.value)
        self.aliases = aliases
        self.data = data
        self.callback_topic = callback_topic

    @staticmethod
    def create_instance(message) -> 'FeaturesRequest':
        return FeaturesRequest(message['request_id'],
                               [Alias.create_instance(alias) for alias in message['aliases']],
                               pd.DataFrame.from_dict(message['data']) if 'data' in message else pd.DataFrame(),
                               message['callback_topic'])

    @staticmethod
    def check_data_message_type(message) -> bool:
        return FeaturesRequest._check_message_type(message, MessageCodes.FEATURES_REQUEST)

    def to_json(self) -> dict:
        return {'request_id': self.request_id,
                'code': self.code,
                'aliases': [alias.to_json() for alias in self.aliases],
                'data': self.data.to_dict(),
                'callback_topic': self.callback_topic
        }


class FeaturesRequestCoordinator(AbstractMessage):
    def __init__(self, request_id: str, features: List[FeatureData], data: pd.DataFrame, callback_topic: str):
        super(FeaturesRequestCoordinator, self).__init__(request_id, MessageCodes.FEATURES_REQUEST_COORDINATOR.value)
        self.features = features
        self.data = data
        self.callback_topic = callback_topic

    @staticmethod
    def create_instance(message) -> 'FeaturesRequestCoordinator':
        return FeaturesRequestCoordinator(message['request_id'],
                               [FeatureData.from_dict(feature_data) for feature_data in message['features']],
                               pd.DataFrame.from_dict(message['data']) if 'data' in message else pd.DataFrame(),
                               message['callback_topic'])

    @staticmethod
    def check_data_message_type(message) -> bool:
        return FeaturesRequest._check_message_type(message, MessageCodes.FEATURES_REQUEST_COORDINATOR)

    def to_json(self) -> dict:
        return {'request_id': self.request_id,
                'code': self.code,
                'features': [feature.to_json() for feature in self.features],
                'data': self.data.to_dict(),
                'callback_topic': self.callback_topic}


class FeaturesResponse(AbstractMessage):
    def __init__(self, request_id: str, entity_name: str, generated_features: List[str], data: pd.DataFrame):
        super(FeaturesResponse, self).__init__(request_id, MessageCodes.FEATURES_RESPONSE.value)
        self.entity_name = entity_name
        self.generated_features = generated_features
        self.data = data

    @staticmethod
    def create_instance(message) -> 'FeaturesResponse':
        return FeaturesResponse(message['request_id'],
                                message['entity_name'],
                                [feature_data for feature_data in message['generated_features']],
                                pd.DataFrame.from_dict(message['data']) if 'data' in message else pd.DataFrame())

    @staticmethod
    def check_data_message_type(message) -> bool:
        return FeaturesRequest._check_message_type(message, MessageCodes.FEATURES_RESPONSE)

    def to_json(self) -> dict:
        return  {
            'request_id': self.request_id,
            'code': self.code,
            'entity_name': self.entity_name,
            'generated_features': self.generated_features,
            'data': self.data.to_dict()
        }


class CreateFeatureRequest(AbstractMessage):
    def __init__(self, feature_data_cache: FeatureDataCache):
        super(CreateFeatureRequest, self).__init__('create_feature_' + '{}_{}_{}_{}_{}'
                                                   .format(feature_data_cache.feature_data.feature_name,
                                                           feature_data_cache.feature_data.feature_version.major_version,
                                                           feature_data_cache.feature_data.feature_version.minor_version,
                                                           feature_data_cache.feature_data.feature_version.minor_minor_version,
                                                           feature_data_cache.feature_data.feature_version.project_code),
                                                   MessageCodes.CREATE_FEATURE_REQUEST.value)
        self.feature_data_cache = feature_data_cache

    @staticmethod
    def create_instance(message) -> 'CreateFeatureRequest':
        return CreateFeatureRequest(FeatureDataCache.from_json(message['feature_data_cache']))

    @staticmethod
    def check_data_message_type(message) -> bool:
        return FeaturesRequest._check_message_type(message, MessageCodes.CREATE_FEATURE_REQUEST)

    def to_json(self) -> dict:
        return {'request_id': self.request_id, 'code': self.code, 'feature_data_cache': self.feature_data_cache.to_json()}
