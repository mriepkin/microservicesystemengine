import pathlib
import importlib


class DynamicImporter:
    @staticmethod
    def import_file(file_path):
        module_name = pathlib.Path(file_path).stem
        module = importlib.import_module(module_name)
        return module
