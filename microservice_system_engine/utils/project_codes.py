from enum import Enum


class ProjectCodes(Enum):
    INVALID = -1
    MY_CREDIT = 0
    CLICK_CREDIT = 1
