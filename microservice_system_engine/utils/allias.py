from typing import List, Dict


class FeatureVersionAlias:
    def __init__(self, feature_name: str, feature_alias: str):
        self.feature_name = feature_name
        self.feature_alias = feature_alias

    @staticmethod
    def from_dict(raw):
        return FeatureVersionAlias(raw['feature_name'], raw['feature_alias'])

    def to_json(self) -> dict:
        return {
            'feature_name': self.feature_name,
            'feature_alias': self.feature_alias
        }


class FeatureImplementationAlias:
    def __init__(self, feature_implementation: int, feature_version_aliases: List[List[FeatureVersionAlias]]):
        self.feature_implementation = feature_implementation
        self.feature_version_aliases = feature_version_aliases
        feature_version_aliases_dicts = []
        for i, feature_version_alias in enumerate(feature_version_aliases):
            feature_version_aliases_dicts.append({})
            for alias in feature_version_alias:
                feature_version_aliases_dicts[i][alias.feature_name] = alias
        self.feature_version_aliases_dicts: List[Dict[str, FeatureVersionAlias]] = feature_version_aliases_dicts

    @staticmethod
    def from_dict(raw):
        version_features = []
        for i, single_feature in enumerate(raw['feature_version_aliases']):
            version_features.append([])
            for single_alias in single_feature:
                version_features[i].append(FeatureVersionAlias.from_dict(single_alias))
        return FeatureImplementationAlias(
            raw['feature_implementation'],
            version_features
        )

    def to_json(self) -> dict:
        feature_version_aliases = []
        for i, feature_version_alias in enumerate(self.feature_version_aliases):
            feature_version_aliases.append([])
            for f in feature_version_alias:
                feature_version_aliases[i].append(f.to_json())

        return {
            'feature_implementation': self.feature_implementation,
            'feature_version_aliases': feature_version_aliases
        }


class FeatureAlias:
    # base_features_aliases -> [[LoanId: LoanId_1_0_0_0], [LoanId: LoanId_1_0_0_1]]
    def __init__(self,
                 feature_name: str,
                 features_implementation_aliases: List[FeatureImplementationAlias]):
        self.feature_name = feature_name
        self.feature_implementations = features_implementation_aliases


    @staticmethod
    def from_dict(raw):
        return FeatureAlias(
            raw['feature_name'],
            [FeatureImplementationAlias.from_dict(feature_implementation) for feature_implementation in raw['feature_implementations']]
        )

    def to_json(self):
        return {
            'feature_name': self.feature_name,
            'feature_implementations': [feature_implementation.to_json() for feature_implementation in self.feature_implementations]
        }


class Alias:
    def __init__(self, transformer_name: str, features: List[FeatureAlias]):
        self.transformer_name = transformer_name
        self.features = features

    @staticmethod
    def create_instance(raw):
        return Alias(raw['transformer_name'],
                     [FeatureAlias.from_dict(feature) for feature in raw['features']])

    def to_json(self):
        return {
            'transformer_name': self.transformer_name,
            'features': [feature.to_json() for feature in self.features]
        }
