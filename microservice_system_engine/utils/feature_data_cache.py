import json
from typing import List
from microservice_system_engine.utils import FeatureData


class FeatureDataCache:
    def __init__(self,
                 feature_data: FeatureData,
                 destination: str,
                 dependencies: List[str]):
        self.feature_data = feature_data
        self.destination = destination
        self.dependencies = dependencies

    @staticmethod
    def from_json(feature_json: dict) -> 'FeatureDataCache':
        feature_data = FeatureData.from_dict(feature_json['feature_data'])
        return FeatureDataCache(
            feature_data,
            feature_json['destination'],
            feature_json['dependencies']
        )

    def equals(self, other_data: 'FeatureDataCache') -> bool:
        return self.feature_data.equals(other_data.feature_data) and \
                self.destination == other_data.destination and \
                self.dependencies == other_data.dependencies

    def to_json(self) -> dict:
        return {
            'feature_data': self.feature_data.to_json(),
            'destination': self.destination,
            'dependencies': self.dependencies
        }
