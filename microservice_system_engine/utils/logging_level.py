from enum import Enum


class LoggingLevel(Enum):
    INVALID = 0
    FATAL = 50000  # Very severe error events that might cause the application to terminate.
    ERROR = 40000  # Error events of considerable importance that will prevent normal program execution, but might still allow the application to continue running.
    WARN = 30000  # Potentially harmful situations of interest to end users or system helpers that indicate potential problems.
    INFO = 20000  # Informational messaging that might make sense to end users and system administrators, and highlight the progress of the application.
    DEBUG = 10000  # Relatively detailed tracing used by application developers.
    DEBUG_HIGH = 7000  # Highly detailed tracing messaging. Produces the most voluminous output.
