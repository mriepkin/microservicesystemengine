from microservice_system_engine.utils.feature_version import FeatureVersion


class FeatureData:
    def __init__(self, transformer_name: str, feature_name: str, feature_version: FeatureVersion):
        self.transformer_name = transformer_name
        self.feature_name = feature_name
        self.feature_version = feature_version

    def equals(self, feature_data: 'FeatureData'):
        return self.transformer_name == feature_data.transformer_name and \
               self.feature_name == feature_data.feature_name and \
               self.feature_version.equal(feature_data.feature_version)

    def to_string(self):
        return '{}_{}_{}'.format(self.transformer_name,
                                       self.feature_name,
                                       self.feature_version.to_string())

    @staticmethod
    def from_dict(feature_data):
        feature_version = FeatureVersion(
            feature_data['feature_version']['major_version'],
            feature_data['feature_version']['minor_version'],
            feature_data['feature_version']['minor_minor_version'],
            feature_data['feature_version']['implementation'],
            feature_data['feature_version']['project_code']
        )
        return FeatureData(feature_data['transformer_name']
                           , feature_data['feature_name']
                           , feature_version)

    @staticmethod
    def from_string(feature_data: str) -> 'FeatureData':
        transformer_name, \
        feature_name, \
        major_version, \
        minor_version, \
        minor_minor_version, \
        implementation, \
        project_code = feature_data.split('_')
        feature_version = FeatureVersion(major_version, minor_version, minor_minor_version, implementation, project_code)
        return FeatureData(transformer_name, feature_name, feature_version)

    def to_json(self) -> dict:
        return {
            'transformer_name': self.transformer_name,
            'feature_name': self.feature_name,
            'feature_version': self.feature_version.to_json()
        }


