from microservice_system_engine.utils.project_codes import ProjectCodes
import json


class FeatureVersion:
    def __init__(self,
                 major_version: int,        # If code was updated significantly
                 minor_version: int,        # Hot fix
                 minor_minor_version: int,  # Base column was updated
                 implementation: int,       # Actual code version
                 project_code: ProjectCodes):
        self.major_version = major_version
        self.minor_version = minor_version
        self.minor_minor_version = minor_minor_version
        self.implementation = implementation
        self.project_code = project_code if type(project_code) is ProjectCodes else ProjectCodes(project_code)

    def equal(self, other_feature_version: 'FeatureVersion') -> bool:
        return self.major_version == other_feature_version.major_version and \
               self.minor_version == other_feature_version.minor_version and \
               self.minor_minor_version == other_feature_version.minor_minor_version and \
               self.implementation == other_feature_version.implementation and \
               self.project_code.value == other_feature_version.project_code.value

    def to_string(self):
        return '_'.join([str(self.major_version),
                         str(self.minor_version),
                         str(self.minor_minor_version),
                         str(self.implementation),
                         str(self.project_code.value)])

    def to_json(self) -> dict:
        return {
            'major_version': self.major_version,
            'minor_version': self.minor_version,
            'minor_minor_version': self.minor_minor_version,
            'implementation': self.implementation,
            'project_code': self.project_code.value,
        }
