from abc import ABC
import json
import inspect
from microservice_system_engine.utils.message_codes import MessageCodes


class AbstractMessage(ABC):
    def __init__(self, request_id: str, code: int):
        self.request_id = request_id
        self.code = code

    @staticmethod
    def create_instance(message) -> 'AbstractMessage':
        pass

    @staticmethod
    def _check_if_data_has_code(data) -> bool:
        return data is not None and ((type(data) is dict and 'code' in data) or issubclass(data.__class__, AbstractMessage))

    @staticmethod
    def _check_message_type(message, message_type: MessageCodes) -> bool:
        return AbstractMessage._check_if_data_has_code(message) and \
               ((type(message) is dict and message['code'] == message_type.value)
                or (inspect.isclass(message) and message.code == message_type.value))

    @staticmethod
    def check_data_message_type(message) -> bool:
        pass

    def to_json(self) -> dict:
        return json.loads(json.dumps(self, default=lambda o: o.__dict__,
                                     sort_keys=True))

