from abc import ABC, abstractmethod
from typing import List


class AbstractDbDataWrapper(ABC):
    @staticmethod
    def to_class_format(raw_data_from_db):
        pass


class AbstractDbConnector(ABC):
    @abstractmethod
    def query_single(self, query_string: str) -> AbstractDbDataWrapper:
        pass

    @abstractmethod
    def query_multiple(self, query_string: str) -> List[AbstractDbDataWrapper]:
        pass

    @abstractmethod
    def insert(self, query_string: str) -> None:
        pass

    @abstractmethod
    def connect(self) -> None:
        pass

    @abstractmethod
    def disconnect(self) -> None:
        pass
