from abc import ABC, abstractmethod
from microservice_system_engine.utils.logging_level import LoggingLevel


class AbstractLoggerConfig:
    def __init__(self, min_log_level: str):
        self.min_log_level = min_log_level


class AbstractLogger(ABC):
    """
    Base class for all loggers
    """
    def __init__(self, config: AbstractLoggerConfig):
        self._min_log_level = AbstractLogger.__string_to_log_level(config.min_log_level)

    def log(self, log_level: LoggingLevel, log_name: str, message: str):
        pass

    @staticmethod
    def __string_to_log_level(log_level: str) -> LoggingLevel:
        log_level = log_level.lower()
        if log_level == 'fatal':
            return LoggingLevel.FATAL
        if log_level == 'error':
            return LoggingLevel.ERROR
        if log_level == 'warn':
            return LoggingLevel.WARN
        if log_level == 'info':
            return LoggingLevel.INFO
        if log_level == 'debug':
            return LoggingLevel.DEBUG
        if log_level == 'debug_high':
            return LoggingLevel.DEBUG_HIGH
        return LoggingLevel.INVALID
