from abc import ABC

from microservice_system_engine.abstract_classes.abstract_message import AbstractMessage
from microservice_system_engine.utils.message_codes import MessageCodes
from typing import Dict


class AbstractMessageHandler(ABC):

    @staticmethod
    def handle(message: dict) -> None:
        pass

class AbstractMessageHandlerManager(ABC):
    _handlers: Dict[MessageCodes, AbstractMessageHandler] = {}

    @staticmethod
    def _handle_message(message: dict) -> None:
        if 'code' in message:
            if message['code'] > MessageCodes.LOWEST_NUMBER.value and message['code'] < MessageCodes.BIGGEST_NUMBER.value:
                casted_code = MessageCodes(message['code'])
                if casted_code in AbstractMessageHandlerManager._handlers:
                    AbstractMessageHandlerManager._handlers[casted_code].handle(message)
                    return
                else:
                    #TODO: log
                    return
        else:
            #TODO: log
            return

    @staticmethod
    def add_handler(message_code: MessageCodes, handler: AbstractMessageHandler):
        AbstractMessageHandlerManager._handlers[message_code] = handler

    def handle_messages(self) -> None:
        pass

