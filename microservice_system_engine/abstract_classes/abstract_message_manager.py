from microservice_system_engine.abstract_classes.abstract_message import AbstractMessage


class AbstractMessageManager:
    def get_listening_destination(self):
        pass

    def consume_messages(self):
        pass

    def send_message(self, topic: str, message: AbstractMessage):
        pass

    def flush_messages(self):
        pass

    def get_message_to_process(self):
        pass