from abc import ABC, abstractmethod
from typing import List, Dict, Tuple
from microservice_system_engine.utils import Alias, FeatureImplementationAlias, FeatureVersionAlias
import pandas as pd


class AbstractFeatureImplementation(ABC):
    def __init__(self, transformer_name: str, implementation: int):
        self.transformer_name = transformer_name
        self.implementation = implementation

    def execute(self, data: pd.DataFrame, additional_data, aliases: Dict[str, FeatureVersionAlias]) -> Tuple[pd.DataFrame, List[str]]:
        pass


class AbstractFeature(ABC):
    # major version - significant code change. Changes storage table
    # minor version - slight code change(hotfix). Remains the same storage table
    # minor minor version - base column was updated (major or minor version)

    def __init__(self, feature_name: str, implementations: List[AbstractFeatureImplementation]):
        self.feature_name = feature_name
        self._implementations_dict = {}
        for implementation in implementations:
            self._implementations_dict[implementation.implementation] = implementation

    def execute(self, data: pd.DataFrame, additional_data, aliases: List[FeatureImplementationAlias]) -> Tuple[pd.DataFrame, List[str]]:
        """
        :param data: dict, where feature value should be inserted
        :param additional_data: some other data, which is required for feature generation
        :return: data dict with feature inserted to it
        """
        generated_features: List[str] = []
        for alias in aliases:
            if alias.feature_implementation in self._implementations_dict.keys():
                for feature_version_alias in alias.feature_version_aliases_dicts:
                    data, temp_columns = self._implementations_dict[alias.feature_implementation].execute(data,
                                                                                                          additional_data,
                                                                                                          feature_version_alias)
                    generated_features += temp_columns
            else:
                # TODO: log
                continue
        return data, generated_features


class AbstractTransformer(ABC):
    """
    Base class for all transformers
    """
    def __init__(self, transformer_name: str, features: List[AbstractFeature]):
        """
        Constructor
        :param prefix: prefix for all the columns given by transformer
        """
        super().__init__()
        self._transformer_name = transformer_name
        if transformer_name.endswith('_'):
            self._transformer_name = transformer_name[:-1]

        self._features: Dict[str, AbstractFeature] = {}
        for feature in features:
            self._features[feature.feature_name] = feature

    def execute(self, data: pd.DataFrame, alias: Alias) -> Tuple[pd.DataFrame, List[str]]:
        """
        Function performs transformation of given dataframes into a result dataframe
        :param dfs: Dict of dataframes
        :param features: list of needed features
        :return: [Resulting dataframe, generated features data]
        """
        generated_features: List[str] = []
        for feature in alias.features:
            transformer_name, feature_name = feature.feature_name.split('_')
            if feature_name in self._features.keys() and self._transformer_name == transformer_name:
                data, temp_columns = self._features[feature_name].execute(data, None, feature.feature_implementations)
                generated_features += temp_columns
            else:
                #TODO: log
                continue
        return data, generated_features

    def get_transformer_name(self):
        return self._transformer_name
