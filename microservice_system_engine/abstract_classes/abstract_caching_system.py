from abc import ABC, abstractmethod


class AbstractCachingSystem(ABC):
    @abstractmethod
    def put(self, unique_key, value):
        pass

    @abstractmethod
    def get(self, unique_key):
        pass

    @abstractmethod
    def delete(self, unique_key):
        pass

