from microservice_system_engine.abstract_classes.abstract_logger import AbstractLogger
from typing import List
from microservice_system_engine.utils.logging_level import LoggingLevel


class AbstractLoggerManager:
    _active_loggers: List[AbstractLogger] = []

    @staticmethod
    def add_logger(logger: AbstractLogger):
        AbstractLoggerManager._active_loggers.append(logger)

    @staticmethod
    def log(log_level: LoggingLevel, log_name: str, message: str):
        for logger in AbstractLoggerManager._active_loggers:
            logger.log(log_level, log_name, message)
        print('{}: {}'.format(log_name, message))